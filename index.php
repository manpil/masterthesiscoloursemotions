<?php
/**
 * Created by PhpStorm.
 * User: Manfred
 * Date: 22.02.2016
 * Time: 09:46
 */

session_start();

class Survey {

    private $surveyId = 0;
    private $userId = 0;
    private $uis = array();
    private $pages = array();
    private $db;
    private $debug = false;
    private $index = 0;

    function __construct() {

        if (!empty($_POST['cancelSurvey'])) {
            session_unset();
            $_REQUEST['action'] = "";
        }

        $this->loadSession();
        $this->connectDB();

        if ($this->debug) {
            $this->userId = 1;
            $this->surveyId = 1;

            if (count($this->pages) == 0) {
                $this->loadPages();
            }

            if (empty($_REQUEST['action'])) {
                $_REQUEST['action'] = "color";
            }

            $this->actions();
        } else {
            $this->actions();
        }
    }

    private function loadSession() {
        $this->userId = $_SESSION['userId'];
        $this->surveyId = $_SESSION['surveyId'];
        $this->pages = $_SESSION['pages'];
        $this->uis = $_SESSION['uis'];
        $this->index = $_SESSION['index'] ? $_SESSION['index'] : 0;
    }
    private function connectDB() {
        
        $DB_host = "localhost";
        $DB_user = "user";
        $DB_pass = "passwort";
        $DB_name = "dbname";

        try
        {
            $this->db = new PDO("mysql:host={$DB_host};dbname={$DB_name};",$DB_user,$DB_pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo "ERROR : ".$e->getMessage();
        }
    }

    public function actions() {

        switch ($_REQUEST['action']) {

            case "start":
                $this->start();
                break;

            case "pause":
                include "templates/header.html";
                include "templates/pause.html";
                break;

            case "page":
                $page = $this->pages[$this->index];

                if (!empty($page)) {
                    include "templates/header.html";
                    include "templates/page.php";
                }
                break;

            case "result":
                $this->savePageResult();
                break;

            case "finish":
                if (isset($_POST['insertEmail'])) {
                    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                        $msg_error = "Bitte geben Sie eine gültige Email Adresse ein.";
                    } else {
                        $this->insertEmail();
                        $msg_success = "Ihre Email Adresse wurde gespeichert. Danke für Ihr Interesse.";
                    }
                }
                include "templates/header.html";
                include "templates/finish.php";
                break;

            default:
                include "templates/header.html";
                include "templates/startpage.php";
                break;
        }
    }

    private function start() {

        $age = (int) $_POST['age'];

        if ($age == 0 || $age < 10 || $age > 110) {
            $msg = "Bitte geben Sie ein gültiges Alter zwischen 10 und 110 Jahren ein!";
            include "templates/header.html";
            include "templates/startpage.php";
        } else {
            // insert into user table
            $stmt = $this->db->prepare("INSERT INTO user(gender,age,time) VALUES(:gender, :age, NOW())");
            $stmt->execute(array(
                ':gender' => $_POST['gender'],
                ':age' => $age
            ));
            $_SESSION['userId'] = $this->db->lastInsertId();

            // insert into survey table
            $stmt = $this->db->prepare("INSERT INTO survey(time_start, user_id) VALUES(NOW(), :user_id)");
            $stmt->execute(array(
                ':user_id' => $_SESSION['userId']
            ));
            $_SESSION['surveyId'] = $this->db->lastInsertId();

            $this->loadPages();

            header("Location: index.php?action=page");
        }
    }

    private function loadPages() {
        // get all uis in random order and save in session
        $sql = "SELECT id, file, type, name, question, gew, sam FROM ui ORDER BY type,RAND()";
        //$sql = "SELECT id, file, type, name, question, gew, sam FROM ui WHERE id = 9";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $this->pages = $_SESSION['pages'] = $stmt->fetchAll();
    }

    private function savePageResult() {
        $_SESSION['index'] = $_REQUEST['index'];
        $gewId = 0;
        $samId = 0;

        // save gew data if exists
        if ($_POST['gew'] == 1) {
            $sql = "INSERT INTO gew (interest, amusement, pride, joy, pleasure, contentment, love, admiration, relief, compassion, sadness, guilt, regret, shame, disappointment, fear, disgust, contempt, hate, anger, none, other) ".
                   "VALUES (:interest,:amusement,:pride,:joy,:pleasure,:contentment,:love,:admiration,:relief,:compassion,:sadness,:guilt,:regret,:shame,:disappointment,:fear,:disgust,:contempt,:hate,:anger,:none,:other)";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':interest' => $_POST['interest'],
                ':amusement' => $_POST['amusement'],
                ':pride' => $_POST['pride'],
                ':joy' => $_POST['joy'],
                ':pleasure' => $_POST['pleasure'],
                ':contentment' => $_POST['contentment'],
                ':love' => $_POST['love'],
                ':admiration' => $_POST['admiration'],
                ':relief' => $_POST['relief'],
                ':compassion' => $_POST['compassion'],
                ':sadness' => $_POST['sadness'],
                ':guilt' => $_POST['guilt'],
                ':regret' => $_POST['regret'],
                ':shame' => $_POST['shame'],
                ':disappointment' => $_POST['disappointment'],
                ':fear' => $_POST['fear'],
                ':disgust' => $_POST['disgust'],
                ':contempt' => $_POST['contempt'],
                ':hate' => $_POST['hate'],
                ':anger' => $_POST['anger'],
                ':none' => $_POST['none'],
                ':other' => "{$_POST['other']}"
            ));
            $gewId = $this->db->lastInsertId();
        }

        // save sam data if exists
        if ($_POST['sam'] == 1) {
            $sql = "INSERT INTO sam (valence, arousal, dominance) VALUES (:valence, :arousal, :dominance)";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':valence' => $_POST['valence'],
                ':arousal' => $_POST['arousal'],
                ':dominance' => $_POST['dominance']
            ));
            $samId = $this->db->lastInsertId();
        }

        $stmt = $this->db->prepare("INSERT INTO result (survey_id, ui_id, gew_id, sam_id, time) VALUES (:survey_id, :ui_id, :gew_id, :sam_id, NOW())");
        $stmt->execute(array(
            ':survey_id' => $this->surveyId,
            ':ui_id' => $_POST['id'],
            ':gew_id' => $gewId,
            ':sam_id' => $samId
        ));

        if ($_REQUEST['index'] < count($this->pages)) {
            header ("Location: index.php?action=pause");
        } else {
            // finish this survey
            $stmt = $this->db->prepare("UPDATE survey SET time_stop = NOW(), finished = 1 WHERE id = :id");
            $stmt->execute(array(
                ':id' => $this->surveyId
            ));

            session_unset();
            header ("Location: index.php?action=finish");
        }
    }

    private function insertEmail() {
        $stmt = $this->db->prepare("INSERT INTO email (email) VALUES (:email)");
        $stmt->execute(array(
            ':email' => $_POST['email']
        ));
    }
}

$survey = new Survey();


require "templates/footer.html";