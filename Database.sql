-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: dd3316.kasserver.com
-- Erstellungszeit: 24. Mai 2016 um 17:11
-- Server Version: 5.5.49-nmm1-log
-- PHP-Version: 5.4.45-nmm1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `d0212780`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `color`
--

CREATE TABLE IF NOT EXISTS `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `hexcode` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gew`
--

CREATE TABLE IF NOT EXISTS `gew` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interest` int(11) NOT NULL,
  `amusement` int(11) NOT NULL,
  `pride` int(11) NOT NULL,
  `joy` int(11) NOT NULL,
  `pleasure` int(11) NOT NULL,
  `contentment` int(11) NOT NULL,
  `love` int(11) NOT NULL,
  `admiration` int(11) NOT NULL,
  `relief` int(11) NOT NULL,
  `compassion` int(11) NOT NULL,
  `sadness` int(11) NOT NULL,
  `guilt` int(11) NOT NULL,
  `regret` int(11) NOT NULL,
  `shame` int(11) NOT NULL,
  `disappointment` int(11) NOT NULL,
  `fear` int(11) NOT NULL,
  `disgust` int(11) NOT NULL,
  `contempt` int(11) NOT NULL,
  `hate` int(11) NOT NULL,
  `anger` int(11) NOT NULL,
  `none` int(11) NOT NULL,
  `other` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1285 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `ui_id` int(11) NOT NULL,
  `gew_id` int(11) NOT NULL,
  `sam_id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1284 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sam`
--

CREATE TABLE IF NOT EXISTS `sam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valence` int(11) NOT NULL,
  `arousal` int(11) NOT NULL,
  `dominance` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_start` datetime NOT NULL,
  `time_stop` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `finished` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ui`
--

CREATE TABLE IF NOT EXISTS `ui` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` text NOT NULL,
  `type` varchar(10) NOT NULL,
  `name` text NOT NULL,
  `question` text NOT NULL,
  `gew` tinyint(1) NOT NULL DEFAULT '0',
  `sam` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` enum('m','f') NOT NULL,
  `age` int(11) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
