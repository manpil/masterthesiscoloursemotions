/**
 * Created by Manfred on 25.02.2016.
 */

function onGEWLoad(svg, error) {
    $('#GEW path').click(function(){
        var emotion = $(this).attr('id').slice(0,-1);
        var score = $(this).attr('id').substring($(this).attr('id').length-1);

        if ($(this).hasClass('selected')){
            $(this).removeClass('selected');
            $('input[name='+emotion+']').removeClass('selected');
            $('input[name='+emotion+']').val('');

            if (emotion == "noEmotio") {
                $('#none').val('0');
            }
        }else{
            $('#GEW').find('path[id^='+emotion+']').removeClass('selected');
            $('input[name='+emotion+']').val(score);
            $(this).addClass('selected');

            if (emotion == "noEmotio") {
                $('#none').val('1');
            }
        }
    });

    $('#GEW path').hover(
        function() {
            $( this ).addClass( "hover" );
        }, function() {
            $( this ).removeClass( "hover" );
        }
    );

    $("input#other").keyup(function(){
        if ($('input#other').val()==""){
            $('#otherEmotion').removeClass('selected');
        }else{
            $('#otherEmotion').addClass('selected');
        }
    });

    $('input#otherEmotion').blur(function(){
        $('input[name=otherEmotion]').val($(this).val());
    });
}

function onLoad_SAM_valence() {
    $('#SAM_valence rect').click(function(){
        //var emotion = $(this).attr('id').slice(0,-1);
        var score = $(this).attr('id').substring($(this).attr('id').length-1);

        var selectedOpacity = "0.3";
        var selectedStroke = "1";

        if ($(this).attr("fill-opacity") == selectedOpacity) {
            $(this).attr("fill-opacity","0");
            $(this).attr("stroke-opacity","0");
            $('#valence').val('');
        } else {
            $('#SAM_valence rect').attr("fill-opacity","0");
            $('#SAM_valence rect').attr("stroke-opacity","0");
            $(this).attr("fill-opacity",selectedOpacity);
            $(this).attr("stroke-opacity",selectedStroke);
            $('#valence').val(score);
        }
    });
};

function onLoad_SAM_arousal() {
    $('#SAM_arousal rect').click(function(){
        //var emotion = $(this).attr('id').slice(0,-1);
        var score = $(this).attr('id').substring($(this).attr('id').length-1);

        var selectedOpacity = "0.3";
        var selectedStroke = "1";

        if ($(this).attr("fill-opacity") == selectedOpacity) {
            $(this).attr("fill-opacity","0");
            $(this).attr("stroke-opacity","0");
            $('#arousal').val('');
        } else {
            $('#SAM_arousal rect').attr("fill-opacity","0");
            $('#SAM_arousal rect').attr("stroke-opacity","0");
            $(this).attr("fill-opacity",selectedOpacity);
            $(this).attr("stroke-opacity",selectedStroke);
            $('#arousal').val(score);
        }
    });
};

function onLoad_SAM_dominance() {
    $('#SAM_dominance rect').click(function(){
        //var emotion = $(this).attr('id').slice(0,-1);
        var score = $(this).attr('id').substring($(this).attr('id').length-1);

        var selectedOpacity = "0.3";
        var selectedStroke = "1";

        if ($(this).attr("fill-opacity") == selectedOpacity) {
            $(this).attr("fill-opacity","0");
            $(this).attr("stroke-opacity","0");
            $('#dominance').val('');
        } else {
            $('#SAM_dominance rect').attr("fill-opacity","0");
            $('#SAM_dominance rect').attr("stroke-opacity","0");
            $(this).attr("fill-opacity",selectedOpacity);
            $(this).attr("stroke-opacity",selectedStroke);
            $('#dominance').val(score);
        }
    });
};

$(function() {
    $('#GEW').svg({
        onLoad: function(){
            var svg = $("#GEW").svg('get');
            svg.load('svg/gew.svg', {addTo: true,  changeSize: false, onLoad: onGEWLoad});
        },
        settings: {}
    });

    $('#SAM_valence').svg({
        onLoad: function(){
            var svg = $("#SAM_valence").svg('get');
            svg.load('svg/sam_valence.svg', {addTo: true,  changeSize: false, onLoad: onLoad_SAM_valence});
        },
        settings: {}
    });

    $('#SAM_arousal').svg({
        onLoad: function(){
            var svg = $("#SAM_arousal").svg('get');
            svg.load('svg/sam_arousal.svg', {addTo: true,  changeSize: false, onLoad: onLoad_SAM_arousal});
        },
        settings: {}
    });

    $('#SAM_dominance').svg({
        onLoad: function(){
            var svg = $("#SAM_dominance").svg('get');
            svg.load('svg/sam_dominance.svg', {addTo: true,  changeSize: false, onLoad: onLoad_SAM_dominance});
        },
        settings: {}
    });

    $("#save").click(function(){
        if ($('#GEW').length > 0 && $('.selected').length < 1){
            $('#gewModal').modal('show');
            return false;
        }

        if (($('#SAM_valence').length > 0 && $('#valence').val() == "") ||
            ($('#SAM_arousal').length > 0 && $('#arousal').val() == "") ||
            ($('#SAM_dominance').length > 0 && $('#dominance').val() == "")) {
                $('#samModal').modal('show');
                return false;
        }

        $('#form').submit();
    });

    $('[data-toggle="popover"]').popover()
});