# Files for Master thesis of Manfred Pilhar

### Folders ###

* **/GEW** contains the german documentation of the Geneva Emotion Wheel
* **/ai** contains the Adobe Illustrator files to create the SVG files
* **/svg** contains the used svg files to display the GEW widget
* **/index.php** PHP code to display the survey
* **/export.php** PHP code to export data from the Database
* **/Database.sql** SQL Export of the database structure