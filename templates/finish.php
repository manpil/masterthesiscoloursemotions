<div class="page">
<h1>Vielen Dank für Ihre Teilnahme</h1>
<p class="lead">Falls Sie Interesse an den Ergebnissen der Umfrage und/oder der fertiggestellten Diplomarbeit haben,<br>
    können Sie gerne optional Ihre Emailadresse hinterlassen</p>
<?
if (!empty($msg_error)) {
    echo "<p class=\"alert alert-danger\">{$msg_error}</p>";
}

if (!empty($msg_success)) {
    echo "<p class=\"alert alert-success\">{$msg_success}</p>";
}
?>
    <form class="form-inline" method="post" action="index.php?action=finish">
        <div class="form-group">
            <input type="email" name="email" class="form-control" id="email" placeholder="Email-Adresse">
        </div>
        <button name="insertEmail" type="submit" class="btn btn-primary">Absenden</button>
    </form>
    <p>&nbsp;<br><b>Datenschutzinformation:</b> Die Emailadresse wird ihrer ausgefüllten Umfrage NICHT zugeordnet. Die Umfrage bleibt somit weiterhin völlig anonym.</p>
</div>