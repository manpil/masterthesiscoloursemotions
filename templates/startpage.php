<div class="starting-page">
    <h1>Farben und Emotionen in Benutzerinterfaces</h1>
    <p class="lead">Willkommen zur Umfrage meiner Diplomarbeit zum Thema Farben und Emotionen in Benutzerinterfaces.<br>
    Sehen Sie sich bitte das kurze Anleitungsvideo an und geben Sie danach ihr Alter und Ihr Geschlecht ein.</p>
    <div>
        <iframe id="video" width="853" height="480" src="https://www.youtube.com/embed/gQhIKF_GgPY" frameborder="0" allowfullscreen></iframe>
    </div>
    <?
    $checked_male = "checked";

    if ($_POST['gender'] == "f") {
        $checked_female = "checked";
        $checked_male = "";
    }
    if (!empty($msg)) {
        echo "<p class=\"alert alert-danger\">{$msg}</p>";
    }
    ?>
    <div style="margin: 0 auto">

    <form action="index.php" method="post" style="text-align: center">
    <b style="font-size: 1.4em">Ich bin</b>
    <div class="radio">
        <label>
            <input type="radio" name="gender" id="gender_male" value="m" <? echo $checked_male; ?>>
            männlich
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="gender" id="gender_female" value="f" <? echo $checked_female; ?>>
            weiblich
        </label>
    </div>
    <div class="select form-inline input-small">
        <input type="number" name="age" class="form-control" id="age" placeholder="" style="width: 90px;"> Jahre alt
    </div>
    <br>

        <h3>Die Umfrage wurde geschlossen, vielen Dank für Ihre Teilnahme</h3>

    <!-- <input class="btn btn-primary" type="submit" value="Starte Umfrage"> -->

    <!-- <input type="hidden" name="action" value="start"> -->
    </form>
    </div>
    <p>&nbsp;</p>
    <div>
        <b>Datenschutzinformation:</b>
        <p>Sämtliche eingegebenen Daten sind anonym. <br>Alter und Geschlecht der Teilnehmer werden nur für statistische Zwecke erfasst.</p>
    </div>
</div>
