<div class="page">
    <form id="formColor" action="index.php" method="post" class="form-inline">
    <?
        $pageNumber = $this->index;
        $allPages = count($this->pages);
        $progress =  (int) (100 / $allPages * $pageNumber);

        if ($pageNumber+1 == $allPages) {
            $buttonNext = "Umfrage fertigstellen";
        } else {
            $buttonNext = "N&auml;chstes Bild &gt;&gt;";
        }
    ?>
        <div class="progress" style="margin-bottom: 5px">
            <div class="progress-bar" role="progressbar" aria-valuenow="<? echo $progress; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $progress; ?>%; min-width: 2em;">
                <? echo $progress; ?>%
            </div>
        </div>
        <div>Betrachten Sie dieses Monitorbild bitte mindestens 5 Sekunden lang und versuchen Sie anschließend die darunterstehende Frage zu beantworten.</div>
    <?
    //echo "<b>".$page['file']."</b>";
    if (file_exists("templates/".$page['file'])) {
        include $page['file'];
    } else {
        echo "<b>".$page['file']."</b> nicht gefunden";
    }
    ?>
        <br>

    <p class="lead">
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#gewHelpModal" style="margin-right: 30px">
        Hilfe anzeigen
        </button>
        <!-- <? echo $pageNumber; ?> von <? echo $allPages; ?>:--> <? echo $page['question']; ?>
        <input id="save" name="saveColor" type="submit" class="btn btn-primary" value="<? echo $buttonNext; ?>" style="margin-left: 30px">
    </p>
    <input type="hidden" name="action" value="result">
    <input type="hidden" name="index" value="<? echo $this->index+1; ?>">
    <input type="hidden" name="id" value="<? echo $page['id']; ?>">
    <?
    if ($page['gew'] == 1) {
        include "gew.html";
    }

    if ($page['sam'] == 1) {
        include "sam.html";
    }
    ?>


    <!-- <form action="index.php" method="post" class="form-inline">
        <input type="submit" class="btn btn-primary" value="&lt;&lt; Zurück">
        <input type="hidden" name="action" value="colorResult">
        <input type="hidden" name="colorIndex" value="<? echo $this->colorIndex-1; ?>">
    </form> -->
<!--
        <input id="reset" name="cancelSurvey" type="submit" class="btn btn-default" value="Neu starten" onClick="return confirm('Wollen Sie wirklich die Umfrage neu starten?')">
-->
        <input type="hidden" name="interest" id="interest" />
        <input type="hidden" name="amusement" id="amusement"/>
        <input type="hidden" name="pride" id="pride"/>
        <input type="hidden" name="joy" id="joy"/>
        <input type="hidden" name="pleasure" id="pleasure"/>
        <input type="hidden" name="contentment" id="contentment"/>
        <input type="hidden" name="love" id="love"/>
        <input type="hidden" name="admiration" id="admiration"/>
        <input type="hidden" name="relief" id="relief"/>
        <input type="hidden" name="compassion" id="compassion"/>
        <input type="hidden" name="sadness" id="sadness"/>
        <input type="hidden" name="guilt" id="guilt"/>
        <input type="hidden" name="regret" id="regret"/>
        <input type="hidden" name="shame" id="shame"/>
        <input type="hidden" name="disappointment" id="disappointment"/>
        <input type="hidden" name="fear" id="fear"/>
        <input type="hidden" name="disgust" id="disgust"/>
        <input type="hidden" name="contempt" id="contempt"/>
        <input type="hidden" name="hate" id="hate"/>
        <input type="hidden" name="anger" id="anger"/>
        <input type="hidden" name="none" id="none" value="0"/>

        <input type="hidden" id="valence" name="valence" />
        <input type="hidden" id="arousal" name="arousal" />
        <input type="hidden" id="dominance" name="dominance" />
    </form>
</div>