<div class="starting-page">
    <div class="uibox" style="background-color: <? echo $color["hexcode"]; ?>">
        <h1>UI UI UI UI</h1>
    </div>
    <p class="lead"><? echo $this->moduleIndex+1; ?> von <? echo count($this->modules); ?>: Welche Emotionen empfinden Sie bei der Betrachtung dieses Benutzerinterfaces?</p>
    <div class="gew_container">
        <div id="GEW" class=""></div>
        <input id="other" type="text" placeholder="Andere"/>
    </div>
    <!-- <form action="index.php" method="post" class="form-inline">
        <input type="submit" class="btn btn-primary" value="&lt;&lt; Zurück">
        <input type="hidden" name="action" value="colorResult">
        <input type="hidden" name="colorIndex" value="<? echo $this->colorIndex-1; ?>">
    </form> -->
    <form id="formColor" action="index.php" method="post" class="form-inline">
        <input id="reset" name="cancelSurvey" type="submit" class="btn btn-default" value="Neu starten">
        <input id="save" name="saveModule" type="submit" class="btn btn-primary" value="Nächstes Bild &gt;&gt;">
        <input type="hidden" name="action" value="moduleResult">
        <input type="hidden" name="colorIndex" value="<? echo $this->moduleIndex+1; ?>">

        <input type="hidden" name="interest" />
        <input type="hidden" name="amusement" />
        <input type="hidden" name="pride" />
        <input type="hidden" name="joy" />
        <input type="hidden" name="pleasure" />
        <input type="hidden" name="contentment" />
        <input type="hidden" name="love" />
        <input type="hidden" name="admiration" />
        <input type="hidden" name="relief" />
        <input type="hidden" name="compassion" />
        <input type="hidden" name="sadness" />
        <input type="hidden" name="guilt" />
        <input type="hidden" name="regret" />
        <input type="hidden" name="shame" />
        <input type="hidden" name="disappointment" />
        <input type="hidden" name="fear" />
        <input type="hidden" name="disgust" />
        <input type="hidden" name="contempt" />
        <input type="hidden" name="hate" />
        <input type="hidden" name="anger" />
        <input type="hidden" name="otherEmotion" />
        <input type="hidden" name="noEmotion" />
    </form>
</div>
