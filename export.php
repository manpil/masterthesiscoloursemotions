<?php
/**
 * Created by PhpStorm.
 * User: Manfred
 * Date: 22.02.2016
 * Time: 09:46
 */

session_start();

class Auswertung
{
    private $db;

    function __construct()
    {
        $this->connectDB();

        switch ($_GET['t']) {
            case "1":
                $this->allData();
                break;

            case "2":
                $this->userData();
                break;

            case "3":
                $this->uiData();
                break;

            case "4":
                $this->timeData();
                break;
        }
    }

    private function connectDB()
    {
        $DB_host = "localhost";
        $DB_user = "dbuser";
        $DB_pass = "password";
        $DB_name = "dbname";

        try {
            $this->db = new PDO("mysql:host={$DB_host};dbname={$DB_name};", $DB_user, $DB_pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "ERROR : " . $e->getMessage();
        }
    }

    private function allData() {
        $sql = "SELECT survey.id AS survey_id, user.id AS user_id, user.age, user.gender FROM survey ".
                "INNER JOIN user ON user.id = survey.user_id ".
                "WHERE survey.finished = :finished";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array(
            ':finished' => 1
        ));
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $data = array();
        foreach($res as $key => $elem) {


            $sql = "SELECT result.ui_id, result.time AS answer_time, gew.* FROM result ".
                    "INNER JOIN gew ON gew.id = result.gew_id ".
                    "WHERE survey_id = :survey_id ".
                    "ORDER BY result.ui_id";

            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':survey_id' => $elem['survey_id']
            ));
            $res2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $elem['results'] = $res2;

            $data[] = $elem;
        }

        print_r($data);
    }

    private function userData() {
        $sql = "SELECT user.id, user.age, COUNT(user.id) AS persons, SUM(CASE WHEN gender = 'm' THEN 1 ELSE 0 END) AS males, SUM(CASE WHEN gender = 'f' THEN 1 ELSE 0 END) AS females FROM survey ".
            "INNER JOIN user ON user.id = survey.user_id ".
            "WHERE survey.finished = :finished ".
            "GROUP BY user.age ORDER BY user.age";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array(
            ':finished' => 1
        ));
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        $str = "Age;Males;Females\n";
        foreach($res as $key => $elem) {
            $str .= $elem['age'].";".$elem['males'].";".$elem['females']."\n";
        }

        echo "<textarea rows='40' cols='80'>{$str}</textarea>";
    }

    private function uiData() {
        $gender = $_GET['g'];

        if ($gender == "m" || $gender == "f") {
            $sql = "SELECT survey.id, user.gender FROM survey ".
                "INNER JOIN user ON (user.id = survey.user_id AND user.gender = :gender) ".
                "WHERE survey.finished = :finished ";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':finished' => 1,
                ':gender' => $gender
            ));
        } else {
            $sql = "SELECT survey.id FROM survey ".
                "WHERE survey.finished = :finished ";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':finished' => 1
            ));
        }

        echo $cnt = $stmt->rowCount();
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $score = array();
        foreach($res as $key => $elem)
        {
            $sql = "SELECT result.ui_id, result.time AS answer_time, ui.name AS name, gew.* FROM result ".
                "INNER JOIN gew ON gew.id = result.gew_id ".
                "INNER JOIN ui ON result.ui_id = ui.id ".
                "WHERE survey_id = :survey_id ".
                "ORDER BY result.ui_id";

            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':survey_id' => $elem['id']
            ));
            $gew_result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach($gew_result as $key => $gew)
            {
                $uiId = $gew['ui_id'];
                $name = $gew['name'];

                if ($gender == "m") $name .= " male";
                if ($gender == "f") $name .= " female";

                $score[$uiId]['name'] = $uiId." ".$name;
                $score[$uiId]['count'] = $cnt;
                $score[$uiId]['interest'] += $gew['interest'];
                $score[$uiId]['amusement'] += $gew['amusement'];
                $score[$uiId]['pride'] += $gew['pride'];
                $score[$uiId]['joy'] += $gew['joy'];
                $score[$uiId]['pleasure'] += $gew['pleasure'];
                $score[$uiId]['contentment'] += $gew['contentment'];
                $score[$uiId]['love'] += $gew['love'];
                $score[$uiId]['admiration'] += $gew['admiration'];
                $score[$uiId]['relief'] += $gew['relief'];
                $score[$uiId]['compassion'] += $gew['compassion'];
                $score[$uiId]['sadness'] += $gew['sadness'];
                $score[$uiId]['guilt'] += $gew['guilt'];
                $score[$uiId]['regret'] += $gew['regret'];
                $score[$uiId]['shame'] += $gew['shame'];
                $score[$uiId]['disappointment'] += $gew['disappointment'];
                $score[$uiId]['fear'] += $gew['fear'];
                $score[$uiId]['disgust'] += $gew['disgust'];
                $score[$uiId]['contempt'] += $gew['contempt'];
                $score[$uiId]['hate'] += $gew['hate'];
                $score[$uiId]['anger'] += $gew['anger'];
                $score[$uiId]['none'] += $gew['none'];
            }
        }

        $str = "";
        $round = 5;
        $perc = 1;

        $str .= "Name;Interesse;Belustigung;Stolz;Freude;Vergn&uuml;gen;Zufriedenheit;Liebe;Bewunderung;Erleichterung;Mitgef&uuml;hl;Trauer;Schuld;Bereuen;Scham;Entt&auml;uschung;Angst;Ekel;Verachtung;Hass;Wut;Keine\n";
        foreach ($score as $ui) {
            $str .= $ui['name'].";".
                number_format(($ui['interest'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['amusement'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['pride'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['joy'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['pleasure'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['contentment'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['love'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['admiration'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['relief'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['compassion'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['sadness'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['guilt'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['regret'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['shame'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['disappointment'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['fear'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['disgust'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['contempt'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['hate'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['anger'] / $cnt) * $perc,$round,",","").";".
                number_format(($ui['none']) * $perc,$round,",","")."\n";
                //number_format(($ui['none'] / $cnt) * $perc,$round,",","")."\n";
        }

        echo "<textarea rows='40' cols='150'>{$str}</textarea>";
    }

    private function timeData() {
        $gender = $_GET['g'];

        if ($gender == "m" || $gender == "f") {
            $sql = "SELECT survey.id, user.gender, TIME_TO_SEC(TIMEDIFF(time_stop, time_start)) AS timesec FROM survey ".
                "INNER JOIN user ON (user.id = survey.user_id AND user.gender = :gender) ".
                "WHERE survey.finished = :finished ";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':finished' => 1,
                ':gender' => $gender
            ));
        } else {
            $sql = "SELECT survey.id, TIME_TO_SEC(TIMEDIFF(time_stop, time_start)) AS timesec, TIMEDIFF(time_stop, time_start) AS time FROM survey ".
                "WHERE survey.finished = :finished ";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':finished' => 1
            ));
        }

        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $i = 0;
        $val = 0;
        foreach($res as $key => $elem)
        {
            $val += $elem['timesec'];
            $i++;
        }

        echo $secs = (int) ($val / $i);

        echo $this->secondsToTime($secs);
        
    }

    function secondsToTime($seconds) {
        $dtF = new DateTime("@0");
        $dtT = new DateTime("@$seconds");
        return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
    }
}
$auswertung = new Auswertung();